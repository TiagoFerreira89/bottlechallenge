#!/bin/sh

cd ~/bottlechallenge
sudo docker build --rm -t tiagoferreira89/bottlechallenge .
sudo docker stop $(sudo docker ps -q --filter ancestor=tiagoferreira89/bottlechallenge) 2>/dev/null
sudo docker run -p 5000:5000 -d -t tiagoferreira89/bottlechallenge
sudo docker logs $(sudo docker ps -q --filter ancestor=tiagoferreira89/bottlechallenge)
