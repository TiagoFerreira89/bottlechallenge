#Set the base image to Ubuntu
FROM ubuntu:16.04

# File Author / Maintainer
MAINTAINER Tiago Ferreira

# Update OS
RUN sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list
RUN apt-get update
RUN apt-get -y upgrade

# Install basic applications
RUN apt-get install -y tar git curl nano wget dialog net-tools build-essential

# Install Python and Basic Python Tools
RUN apt-get install -y python python-dev python-distribute python-pip

# Install bottle framework

RUN apt-get install -y python-bottle
RUN pip install bottle

# Install Python and Basic Python Tools
RUN apt-get install -y python python-dev python-distribute python-pip

# Copy the application folder inside the container
COPY ./BottleChallenge /BottleChallenge/
WORKDIR /BottleChallenge

# Expose ports
EXPOSE 5000 

# Set the default directory where CMD will execute
CMD python challenge.py
