import os
from bottle import Bottle, run

app = Bottle()


@app.route('/bottleChallenge')
def bottleChallenge():
    return load_file()


def load_file():
    file = open("testfile.txt", "r")
    for line in file:
    print line


if __name__ == '__main__':
    run(app, host='0.0.0.0', port=5000, debug=True)
